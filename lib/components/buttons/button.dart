import 'package:flutter/material.dart';

import '../../helpers/helpers.dart';

class ButtonAuth extends StatelessWidget {
  const ButtonAuth({
    Key? key,
    required this.onPressed,
    required this.widgetText,
  }) : super(key: key);

  final Function() onPressed;
  final Widget widgetText;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: MediaQuery.of(context).size.width * 0.9,
      decoration: BoxDecoration(
        color: primaryColor,
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: TextButton(
        onPressed: onPressed,
        child: widgetText,
      ),
    );
  }
}
