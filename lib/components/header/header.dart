import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
    required this.text,
    required this.height,
    required this.width,
    required this.containerColor,
    required this.textColor,
    this.onTap,
  }) : super(key: key);
  final String text;
  final double height;
  final double width;
  final Color containerColor;
  final Color? textColor;
  final Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap??(){},
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            text,
            style: GoogleFonts.poppins(
              color: textColor,
              fontSize: 18.0,
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            width: width,
            height: height,
            color: containerColor,
          )
        ],
      ),
    );
  }
}
