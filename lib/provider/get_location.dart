import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tracker/helpers/endpoint.dart';
import 'package:http/http.dart' as http;
import 'package:tracker/models/location.dart';

import '../models/http_exception.dart';
import '../services/get_user.dart';

class GetCurrentLocation with ChangeNotifier {
  List<TrackLocation> _dataLocation = [];

  UnmodifiableListView<TrackLocation> get dataLocation {
    return UnmodifiableListView(_dataLocation);
  }

  Future<void> getPosition() async {
    final getTokenIn = await getUser();

    Geolocator.getPositionStream().listen((location) async {
      final url = Uri.parse(
          "$endPoint/track?address=${location.latitude}/${location.longitude}");
      final response = await http.get(url, headers: {
        "ContentType": "application/json",
        "Authorization": "Bearer ${getTokenIn.first}",
      });

      final track = await json.decode(response.body) as Map<String, dynamic>;

      print(track["data"]);
      if (track["data"] == null) {
        return;
      }

      List<TrackLocation> extractedDataLocation = [];

      for (var element in track["data"] as List) {
        extractedDataLocation.add(TrackLocation(
          trackModule: element["track"],
          distance: element["dist"],
          myPosition: element["my_position"],
        ));
      }
      _dataLocation = extractedDataLocation;
      // print(extractedDataLocation[0].trackModule["infra_status"]);

      if (track["error"] != null) {
        throw HttpException("Something went wrong");
      }
      notifyListeners();
    });

    //     .getCurrentPosition(
    //   desiredAccuracy: LocationAccuracy.high,
    // );
    //
    // try {
    //   final response = await http.get(url, headers: {
    //     "ContentType": "application/json",
    //     "Authorization": "Bearer ${getTokenIn.first}",
    //   });
    //
    //   final track = await json.decode(response.body) as Map<String, dynamic>;
    //
    //   print(track["data"]);
    //   if (track["data"] == null) {
    //     return;
    //   }
    //
    //   List<TrackLocation> extractedDataLocation = [];
    //
    //   for (var element in track["data"] as List) {
    //     extractedDataLocation.add(TrackLocation(
    //       trackModule: element["track"],
    //       distance: element["dist"],
    //       myPosition: element["my_position"],
    //     ));
    //   }
    //   _dataLocation = extractedDataLocation;
    //   // print(extractedDataLocation[0].trackModule["infra_status"]);
    //
    //   if (track["error"] != null) {
    //     throw HttpException("Something went wrong");
    //   }
    //
    //   notifyListeners();
    // } catch (error) {
    //   rethrow;
    // }
    notifyListeners();
  }

  Future<void> playAlarm(double distance) async {

    final prefs = await SharedPreferences.getInstance();
    prefs.setDouble("distance", distance);

    if (dataLocation.isNotEmpty) {

      final newDistance = prefs.getDouble("distance");

      if (dataLocation[0].distance * 1000 > newDistance!) {
        // FlutterRingtonePlayer.play(
        //   android: AndroidSounds.notification,
        //   ios: IosSounds.glass,
        //   looping: true,
        //   volume: 0.1,
        //   asAlarm: false,
        // );
        FlutterRingtonePlayer.playAlarm();
      }
    }
    // notifyListeners();
  }

  Future<void> stopAlarm() async {
    FlutterRingtonePlayer.stop();
    notifyListeners();
  }
}
