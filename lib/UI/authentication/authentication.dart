import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import '../../components/buttons/button.dart';
import '../../components/form-input/form_input.dart';
import '../../components/header/header.dart';
import '../../helpers/helpers.dart';
import '../../provider/authentication_provider.dart';

class AuthenticationScreen extends StatefulWidget {
  const AuthenticationScreen({Key? key}) : super(key: key);

  @override
  State<AuthenticationScreen> createState() => _AuthenticationScreenState();
}

enum AuthenticationMode { signUp, login }

class _AuthenticationScreenState extends State<AuthenticationScreen>
    with SingleTickerProviderStateMixin {
  AuthenticationMode _mode = AuthenticationMode.login;

  final GlobalKey<FormState> _formKey = GlobalKey();

  late AnimationController _controller;
  late Animation<double> _opacity;
  late Animation<Offset> _slider;

  final name = TextEditingController;
  final email = TextEditingController;
  final password = TextEditingController;
  final number = TextEditingController;

  bool isChecked = false;
  bool _loading = false;

  final Map<String, String> _loginAuth = {"email": "", "password": ""};
  final Map<String, String> _signupAuth = {
    "name": "",
    "email": "",
    "password": "",
    "number": ""
  };

  Future<void> _onSubmit() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }
    _formKey.currentState!.save();
    setState(() {
      _loading = true;
    });
    try {
      if (_mode == AuthenticationMode.login) {
        await Provider.of<AuthenticationProvider>(context, listen: false).login(
          _loginAuth["email"],
          _loginAuth["password"],
        );
      } else {
        await Provider.of<AuthenticationProvider>(context, listen: false)
            .signup(
          _signupAuth["name"],
          _signupAuth["email"],
          _signupAuth["password"],
          _signupAuth["number"],
        );
      }
    } on HttpException catch (error) {
      _showFlutterToast(error.toString());
    } catch (error) {
      //display message if internet connection is not
      // print("No internet");
    }

    setState(() {
      _loading = false;
    });
  }

  void _showFlutterToast(String message) {
    Fluttertoast.showToast(
      timeInSecForIosWeb: 5,
      msg: message,
      backgroundColor: CupertinoColors.systemRed,
      textColor: Colors.white,
      fontSize: 14.0,
      gravity: ToastGravity.BOTTOM,
      toastLength: Toast.LENGTH_LONG,
    );
  }

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    _opacity = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeIn,
      ),
    );

    _slider = Tween<Offset>(
      begin: const Offset(0, -1.5),
      end: const Offset(0, 0),
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.fastOutSlowIn,
      ),
    );
    _determinePosition();
    super.initState();
  }

  Future<void> _determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 100.0,
            left: 20.0,
            right: 20.0,
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    _mode == AuthenticationMode.login ? "Login" : "Sign Up",
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.w300,
                      fontSize: 30.0,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "By signing up you are agreeing",
                    style: GoogleFonts.poppins(
                      height: 3.0,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w200,
                      color: Colors.black,
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Text(
                    "our Term and privacy policy",
                    style: TextStyle(
                      height: 2,
                      color: Color(0xFF0286D0),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 25.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Header(
                    onTap: () {
                      setState(() {
                        _mode = AuthenticationMode.login;
                        _controller.reverse();
                        // _changeMode();
                      });
                    },
                    text: "Login",
                    width: 40.0,
                    height: 2,
                    textColor: _mode == AuthenticationMode.login
                        ? primaryColor
                        : Colors.grey,
                    containerColor: _mode == AuthenticationMode.login
                        ? primaryColor
                        : Colors.white,
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Header(
                    onTap: () {
                      setState(() {
                        _mode = AuthenticationMode.signUp;
                        _controller.forward();
                        // _changeMode();
                      });
                    },
                    text: "Register",
                    width: 40.0,
                    height: 2,
                    textColor: _mode == AuthenticationMode.signUp
                        ? primaryColor
                        : Colors.grey,
                    containerColor: _mode == AuthenticationMode.signUp
                        ? primaryColor
                        : Colors.white,
                  ),
                ],
              ),
               SizedBox(height: MediaQuery.of(context).size.height * 0.06),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    _mode == AuthenticationMode.signUp
                        ? SlideTransition(
                            position: _slider,
                            child: FadeTransition(
                              opacity: _opacity,
                              child: FormInput(
                                label: "Name",
                                validatorEntry: (value) {
                                  if (value!.isEmpty ) {
                                    return "Name can't be empty";
                                  }else if(value.length < 4){
                                    return "Name will have min 4 characters";
                                  }
                                  //return null;
                                },
                                saveEntry: (value) {
                                  _signupAuth["name"] = value!;
                                },
                                suffixIcon: const Icon(
                                  Icons.account_box_outlined,
                                  color: Colors.grey,
                                ),
                                obscure: false,
                              ),
                            ),
                          )
                        : const SizedBox(
                            height: 0.0,
                          ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    FormInput(
                      label: "Email Address",
                      obscure: false,
                      validatorEntry: (value) {
                        if (value!.isEmpty || !value.contains("@")) {
                          return "invalid email";
                        }
                      },
                      saveEntry: (value) {
                        _signupAuth["email"] = value!;
                        _loginAuth["email"] = value;
                      },
                      suffixIcon: const Icon(
                        Icons.mail_outline_outlined,
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    FormInput(
                      label: "Password",
                      obscure: true,
                      validatorEntry: (value) {
                        if (value!.isEmpty) {
                          return "Password is required";
                        }
                        return null;
                      },
                      saveEntry: (value) {
                        _mode == AuthenticationMode.signUp
                            ? _signupAuth["password"] = value!
                            : _loginAuth["password"] = value!;
                      },
                      suffixIcon: const Icon(
                        Icons.lock_outline_rounded,
                        color: Colors.grey,
                      ),
                    ),
                    const SizedBox(
                      height: 20.0,
                    ),
                    _mode == AuthenticationMode.signUp
                        ? SlideTransition(
                            position: _slider,
                            child: FadeTransition(
                              opacity: _opacity,
                              child: FormInput(
                                label: "Number",
                                obscure: false,
                                validatorEntry: (value) {
                                  if(value!.isEmpty){
                                    return "Number field can't be empty";
                                  }else if(value.length != 9){
                                    return "9 characters";
                                  }
                                  //return null;
                                },
                                saveEntry: (value) {
                                  _signupAuth["number"] = value!;
                                },
                                suffixIcon: const Icon(
                                  Icons.phone_android_outlined,
                                  color: Colors.grey,
                                ),
                              ),
                            ),
                          )
                        : const SizedBox(
                            height: 0.0,
                          ),
                    const SizedBox(
                      height: 25,
                    ),
                    ButtonAuth(
                      onPressed: () {
                        _onSubmit();
                      },
                      widgetText: _loading
                          ? const CircularProgressIndicator(
                        backgroundColor: Colors.white,
                        color: Colors.white,
                      )
                          : Text(
                        _mode == AuthenticationMode.signUp
                            ? "Sign Up"
                            : "Login",
                        style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),
                    _mode == AuthenticationMode.login
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Checkbox(
                                  value: isChecked,
                                  onChanged: (bool? value) {
                                    setState(() {
                                      isChecked = value!;
                                    });
                                  }),
                              const Text('Remember password'),
                            ],
                          )
                        : const SizedBox(
                            height: 0.0,
                          )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
