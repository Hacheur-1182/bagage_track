import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:tracker/provider/get_location.dart';

import '../../components/buttons/mapIcons.dart';
import '../../helpers/helpers.dart';
import '../../routes/app_route.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  bool _isLoading = false;
  final distance = TextEditingController();
  final CameraPosition _initialLocation = const CameraPosition(
    target: LatLng(37.785834, -122.406417),
  );
  final Completer<GoogleMapController> _controller = Completer();

  // Method for retrieving the current location
  Future<void> _getMyPosition() async {
    final location = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
    final GoogleMapController controller = await _controller.future;
    await controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
          target: LatLng(location.latitude, location.longitude),
          zoom: 18.0,
        ),
      ),
    );
  }

  //Zoom in camera
  _zoomIn() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.zoomIn(),
    );
  }

  _zoomOut() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.zoomOut(),
    );
  }

  Future<void> getData() async {
    try {
      InternetAddress.lookup("google.com").then((result) {
        if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
          context.read<GetCurrentLocation>().getPosition().then((_) => {
                if (mounted)
                  {
                    setState(() {
                      _isLoading = false;
                    })
                  }
              });
        }
      }).catchError((onError) {
        Navigator.pushNamed(context, AppRoutes.connectionErrorPage);
        Fluttertoast.showToast(
          msg: "Sorry, No internet connection",
          backgroundColor: Colors.redAccent,
          textColor: Colors.white,
        );
      });
    } on SocketException catch (_) {
      Navigator.pushNamed(context, AppRoutes.connectionErrorPage);
      Fluttertoast.showToast(
        msg: "Sorry, No internet connection",
        backgroundColor: Colors.redAccent,
        textColor: Colors.white,
      );
    }

    setState(() {
      _isLoading = true;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getMyPosition();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    final dataLocation =
        Provider.of<GetCurrentLocation>(context, listen: true).dataLocation;

    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          final GoogleMapController controller = await _controller.future;
          controller.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(
                target: LatLng(
                  dataLocation[0].myPosition["lat"],
                  dataLocation[0].myPosition["long"],
                ),
                zoom: 18.0,
              ),
            ),
          );
        },
        backgroundColor: primaryColor,
        child: const Icon(Icons.my_location),
      ),
      extendBody: true,
      body: _isLoading
          ? const Center(
              child: CircularProgressIndicator(
                color: primaryColor,
              ),
            )
          : Padding(
              padding: const EdgeInsets.only(
                left: 10.0,
                right: 10.0,
                top: 70,
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.15,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 30.0),
                                child: Row(
                                  children: [
                                    const Text(
                                      "Module connected",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18.0,
                                      ),
                                    ),
                                    const Spacer(),
                                    Container(
                                      height: 20,
                                      width: 20,
                                      decoration: BoxDecoration(
                                        color: dataLocation[0]
                                                .trackModule["infra_status"]
                                            ? const Color(0xFF55F578)
                                            : Colors.red,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                  Expanded(
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.7,
                            width: double.infinity,
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              color: const Color(0xFFF4F4F4),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Stack(
                              children: [
                                GoogleMap(
                                  initialCameraPosition: _initialLocation,
                                  myLocationEnabled: true,
                                  myLocationButtonEnabled: false,
                                  markers: {
                                    Marker(
                                      markerId: const MarkerId("my position"),
                                      position: LatLng(
                                        dataLocation[0].myPosition['lat'],
                                        dataLocation[0].myPosition['long'],
                                      ),
                                      infoWindow: const InfoWindow(
                                        title: 'position',
                                      ),
                                      icon: BitmapDescriptor.defaultMarker,
                                    ),
                                    Marker(
                                      markerId: const MarkerId("module"),
                                      position: LatLng(
                                        dataLocation[0].trackModule['lat'],
                                        dataLocation[0].trackModule['long'],
                                      ),
                                      infoWindow: const InfoWindow(
                                        title: 'module',
                                      ),
                                      icon: BitmapDescriptor.defaultMarker,
                                    )
                                  },
                                  // circles: {
                                  //   Circle(
                                  //     circleId: const CircleId("value"),
                                  //     center: LatLng(
                                  //       dataLocation[0].myPosition['lat'],
                                  //       dataLocation[0].myPosition['long'],
                                  //     ),
                                  //     radius: 40.0,
                                  //     fillColor: Colors.black,
                                  //   )
                                  // },
                                  polylines: {
                                    Polyline(
                                      polylineId:
                                          const PolylineId("myPosition"),
                                      points: [
                                        LatLng(
                                          dataLocation[0].myPosition['lat'],
                                          dataLocation[0].myPosition['long'],
                                        ),
                                        LatLng(
                                          dataLocation[0].trackModule['lat'],
                                          dataLocation[0].trackModule['long'],
                                        ),
                                      ],
                                      color: primaryColor,
                                      jointType: JointType.bevel,
                                    ),
                                  },
                                  mapType: MapType.normal,
                                  zoomGesturesEnabled: true,
                                  zoomControlsEnabled: false,
                                  onMapCreated:
                                      (GoogleMapController controller) {
                                    _controller.complete(controller);
                                  },
                                ),
                                SafeArea(
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 10.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        MapIcons(
                                          btnIcon: Icons.add,
                                          tapZoomIcon: _zoomIn,
                                        ),
                                        const SizedBox(height: 20),
                                        MapIcons(
                                          btnIcon: Icons.remove,
                                          tapZoomIcon: _zoomOut,
                                        ),
                                        const SizedBox(height: 30),
                                        IconButton(
                                          onPressed: () {
                                            Navigator.pushReplacementNamed(
                                              context,
                                              AppRoutes.homePage,
                                            );
                                          },
                                          icon: const Icon(
                                            CupertinoIcons.refresh,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SafeArea(
                                  child: Align(
                                    alignment: Alignment.topCenter,
                                    child: Column(
                                      children: [
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.07,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.5,
                                          decoration: BoxDecoration(
                                            color:
                                                primaryColor.withOpacity(0.3),
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                              horizontal: 8.0,
                                            ),
                                            child: Row(
                                              children: [
                                                IconButton(
                                                  onPressed: () {
                                                    // setState(() {
                                                    Provider.of<GetCurrentLocation>(
                                                            context,
                                                            listen: false)
                                                        .stopAlarm();
                                                    // FlutterRingtonePlayer.stop();
                                                    // });
                                                  },
                                                  icon: const Icon(
                                                    Icons.stop_circle_sharp,
                                                    color: Colors.redAccent,
                                                  ),
                                                  iconSize: 30.0,
                                                ),
                                                SizedBox(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.03,
                                                ),
                                                Text(
                                                  "${dataLocation[0].distance.toStringAsFixed(2)} Km",
                                                  style: const TextStyle(
                                                    fontSize: 18.0,
                                                    fontWeight: FontWeight.w900,
                                                    color: Colors.pink,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                        const SizedBox(
                                          height: 10.0,
                                        ),
                                        Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.07,
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.5,
                                          decoration: BoxDecoration(
                                            color:
                                                Colors.white.withOpacity(0.7),
                                            borderRadius:
                                                BorderRadius.circular(15.0),
                                          ),
                                          child: TextFormField(
                                            controller: distance,
                                            onFieldSubmitted: (submit) {
                                              Provider.of<GetCurrentLocation>(
                                                      context,
                                                      listen: false)
                                                  .playAlarm(
                                                double.parse(submit),
                                              );
                                            },
                                            cursorColor: Colors.grey,
                                            decoration: InputDecoration(
                                              label: const Text(
                                                "Enter distance en mètre",
                                                style: TextStyle(
                                                  color: Colors.grey,
                                                ),
                                              ),
                                              focusColor: Colors.grey,
                                              enabledBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                  color: Colors.grey,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                  color: Colors.grey,
                                                ),
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
