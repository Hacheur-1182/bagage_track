import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:tracker/UI/home/home.dart';
import 'package:tracker/connexionStatus/handling_connection.dart';
import 'package:tracker/provider/authentication_provider.dart';
import 'package:tracker/provider/get_location.dart';
import 'package:tracker/routes/app_route.dart';

import 'UI/authentication/authentication.dart';
import 'helpers/custom_route_transition.dart';
import 'helpers/helpers.dart';

Future<void> main() async {
  // WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  // FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  // FlutterNativeSplash.remove();
  if (defaultTargetPlatform == TargetPlatform.android) {
    AndroidGoogleMapsFlutter.useAndroidViewSurface = true;
  }
  runApp(const TrackApp());
}

class TrackApp extends StatelessWidget {
  const TrackApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => AuthenticationProvider(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => GetCurrentLocation(),
        ),
      ],
      child: Consumer<AuthenticationProvider>(
        builder: (BuildContext context, authenticated, Widget? child) {
          return MaterialApp(
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              visualDensity: VisualDensity.adaptivePlatformDensity,
              textTheme: GoogleFonts.poppinsTextTheme(
                Theme.of(context).textTheme.copyWith(
                      bodyText1: GoogleFonts.poppins(
                        textStyle: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
              ),
              pageTransitionsTheme: PageTransitionsTheme(
                builders: {
                  TargetPlatform.android: CustomPageTransitionBuilder(),
                  TargetPlatform.iOS: CustomPageTransitionBuilder(),
                },
              ),
            ),
            home: authenticated.isAuthenticated
                ? const Home()
                : FutureBuilder(
                    initialData: authenticated.tryAutoLogin(),
                    future: authenticated.tryAutoLogin(),
                    builder: (BuildContext context,
                            AsyncSnapshot<dynamic> snapshot) =>
                        snapshot.connectionState == ConnectionState.waiting
                            ? const Scaffold(
                              body: Center(
                                child: CircularProgressIndicator(
                                    color: primaryColor,
                                    backgroundColor: Colors.white,
                                  ),
                              ),
                            )
                            : const AuthenticationScreen(),
                  ),
            routes: {
              AppRoutes.homePage : (ctx) => const Home(),
              AppRoutes.connectionErrorPage : (ctx) => const HandlingConnectionState(),
            },
          );
        },
      ),
    );
  }
}
